"use strict";

angular
    .module("gmsecWebsocket")
    .factory("WebSocket", WebSocket);


function WebSocket() {
    var messages = [];
    var observerCallbacks = {};
    
    // initialize
    var socket = new SockJS("/gmsec/messages");
    var stompClient = Stomp.over(socket);
    stompClient.connect({}, function(frame) {
        stompClient.subscribe('/topic/messages', addMessage);
    });
    
    function addMessage(message){
        var parsedMsg = JSON.parse(JSON.parse(message.body).message).MESSAGE;
        messages.push(parsedMsg);
        var keys = Object.keys(observerCallbacks);
        keys.forEach(function(callbackId) {
            observerCallbacks[callbackId](messages);
        });
    }
    
    return {
        
        registerObserver: function(callback) {
            var callbackId = Date.now();
            
            observerCallbacks["callback" + callbackId] = callback;
            return callbackId;
        },
        
        removeObserver: function(callbackId) {
            delete observerCallbacks[callbackId];
        }
    };
}

