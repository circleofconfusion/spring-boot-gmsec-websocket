"use strict";

angular
    .module("gmsecWebsocket")
    .controller("MessageController", MessageController);
    
MessageController.$inject = ["$scope", "WebSocket"];
    
function MessageController($scope, WebSocket) {
    var vm = this;
    
    vm.messages = [];
    
    var callbackId = WebSocket.registerObserver(updateMessages);
    
    $scope.$on("$destroy", WebSocket.removeObserver(callbackId));
    
    function updateMessages(messages) {
        $scope.$apply(function() {
            vm.messages = messages;
        });
    }
}

